package com.example.stockservice.serviceimpl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.stockservice.constants.AppConstants;
import com.example.stockservice.dto.StockRequestDto;
import com.example.stockservice.dto.StockResponseDto;
import com.example.stockservice.entity.Stock;
import com.example.stockservice.repository.StockRepository;

@SpringBootTest
class StockExchangeServiceImplTest {

	@Mock
	private StockRepository stockRepository;
	
	@InjectMocks
	private StockExchangeServiceImpl stockExchangeServiceImpl;

	@Test
	public void testGetAllStocks() {
		List<Stock> stockList = new ArrayList<Stock>();
		Stock stock = new Stock();
		stock.setStockName("SBI");
		stockList.add(stock);
		Mockito.when(stockRepository.findAll()).thenReturn(stockList );
		
		StockResponseDto responseDto = stockExchangeServiceImpl.getAllStocks();
		Assertions.assertEquals("SBI", responseDto.getStockList().get(0).getStockName());
		
		
	}
	
	@Test
	public void testUpdateStock() {
		StockRequestDto stockRequestDto = new StockRequestDto();
		stockRequestDto.setTransactionType("buy");
		stockRequestDto.setStockId(1l);
		Stock stock = new Stock();
		stock.setStockName("SBI");
		Optional<Stock> optionalStock = Optional.of(stock);
		Mockito.when(stockRepository.findById(Mockito.anyLong())).thenReturn(optionalStock );
		String message = stockExchangeServiceImpl.updateStock(stockRequestDto);
		Assertions.assertEquals(AppConstants.SUCCESS, message);

	}
	
	@Test
	public void testUpdateStockOne() {
		StockRequestDto stockRequestDto = new StockRequestDto();
		stockRequestDto.setTransactionType("sell");
		stockRequestDto.setStockId(1l);
		Stock stock = new Stock();
		stock.setStockName("SBI");
		Optional<Stock> optionalStock = Optional.of(stock);
		Mockito.when(stockRepository.findById(Mockito.anyLong())).thenReturn(optionalStock );
		String message = stockExchangeServiceImpl.updateStock(stockRequestDto);
		Assertions.assertEquals(AppConstants.SUCCESS, message);

	}

}
