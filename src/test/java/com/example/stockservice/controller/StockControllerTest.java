package com.example.stockservice.controller;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.stockservice.constants.AppConstants;
import com.example.stockservice.dto.StockDto;
import com.example.stockservice.dto.StockRequestDto;
import com.example.stockservice.dto.StockResponseDto;
import com.example.stockservice.serviceimpl.StockExchangeServiceImpl;

@SpringBootTest
class StockControllerTest {
	
	@Mock
	StockExchangeServiceImpl stockExchangeServiceImpl;
	
	@InjectMocks
	StockController stockController;
	
	public void setUp() {
		
	}

	@Test
	public void testFetAllStocks() {
		StockResponseDto stockResponseDto = new StockResponseDto();
		List<StockDto> stockList = new ArrayList<StockDto>();
		StockDto stockDto = new StockDto();
		stockDto.setStockName("SBI");
		stockList.add(stockDto);
		stockResponseDto.setStockList(stockList );
		Mockito.when(stockExchangeServiceImpl.getAllStocks()).thenReturn(stockResponseDto );
		ResponseEntity<StockResponseDto> responseEntity = stockController.getAllStocks();
		Assertions.assertEquals("SBI", responseEntity.getBody().getStockList().get(0).getStockName());;

	}
	
	@Test
	public void testUpdateStock() {
		String message = AppConstants.SUCCESS;
		StockRequestDto stockRequestDto = new StockRequestDto();
		Mockito.when(stockExchangeServiceImpl.updateStock(Mockito.any(StockRequestDto.class))).thenReturn(message);
		ResponseEntity<String> response = stockController.updateStock(stockRequestDto);
		Assertions.assertEquals(AppConstants.SUCCESS, response.getBody());;

	}
	
	
	

}
