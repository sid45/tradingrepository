package com.example.stockservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.stockservice.dto.StockRequestDto;
import com.example.stockservice.dto.StockResponseDto;
import com.example.stockservice.exception.QuantityInSufficientException;
import com.example.stockservice.serviceimpl.StockExchangeServiceImpl;

import io.swagger.annotations.ApiOperation;

/**
 * This is controller class used to get  list of all stocks.
 * @author sidramesh Mudhol
 *
 */
@RestController
@RequestMapping("/stock")
public class StockController {
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(StockExchangeServiceImpl.class);
	
	@Autowired
	private StockExchangeServiceImpl stockExchangeServiceImpl;
	
	/**
	 * getAllStocks method is used to get list of all stocks
	 * @return ResponseEntity<StockResponseDto> , may not be null.
	 */
	@GetMapping("")
	@ApiOperation("list all stocks")
	public ResponseEntity<StockResponseDto> getAllStocks() {
		LOGGER.info("inside getAllStocks() method");
		StockResponseDto stockResponseDto = stockExchangeServiceImpl.getAllStocks();
		LOGGER.info("leaving getAllStocks() method");
		return new ResponseEntity<>(stockResponseDto, HttpStatus.OK);
		
	}
	
	/**
	 * 
	 * @param stockRequestDto
	 * @return ResponseEntity<String>, may not be null.
	 * @throws QuantityInSufficientException 
	 */
	@PutMapping("")
	@ApiOperation("update stock")
	public ResponseEntity<String> updateStock(@RequestBody StockRequestDto stockRequestDto)  {
		LOGGER.info("inside updateStock() method");
		String message = stockExchangeServiceImpl.updateStock(stockRequestDto);
		LOGGER.info("leaving updateStock() method");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
