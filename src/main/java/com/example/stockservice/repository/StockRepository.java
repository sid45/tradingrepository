package com.example.stockservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.stockservice.entity.Stock;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {

}
