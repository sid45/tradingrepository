package com.example.stockservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.stockservice.constants.AppConstants;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(value=StockNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleNotFoundException() {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(AppConstants.STOCK_NOT_FOUND);
		errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value=QuantityInSufficientException.class)
	public ResponseEntity<ErrorResponse> handleQuantityInsufficientException() {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(AppConstants.QUANTITY_INSUFFICIENT);
		errorResponse.setStatus(620);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}
	
	
	

}
