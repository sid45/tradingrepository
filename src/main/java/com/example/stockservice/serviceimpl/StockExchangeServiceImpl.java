package com.example.stockservice.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.stockservice.constants.AppConstants;
import com.example.stockservice.dto.StockDto;
import com.example.stockservice.dto.StockRequestDto;
import com.example.stockservice.dto.StockResponseDto;
import com.example.stockservice.entity.Stock;
import com.example.stockservice.exception.QuantityInSufficientException;
import com.example.stockservice.exception.StockNotFoundException;
import com.example.stockservice.repository.StockRepository;
import com.example.stockservice.service.StockService;

/**
 * 
 * @author sidramesh mudhol
 * this class is service class used to get list of all stocks
 *
 */
@Service
public class StockExchangeServiceImpl implements StockService {

	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(StockExchangeServiceImpl.class);

	@Autowired
	private StockRepository stockRepository;
	
	
	/**
	 * this method is used to get list of all stocks
	 * StockResponseDto may not be null.
	 */
	@Override
	public StockResponseDto getAllStocks() {
		LOGGER.info("inside getAllStocks() method");
		StockResponseDto stockResponseDto = new StockResponseDto();
		List<StockDto> stockDtoList = new ArrayList<StockDto>();
		List<Stock> stockList = (List<Stock>) stockRepository.findAll();
		for (Stock stock : stockList) {
			StockDto stockDto = new StockDto();
			BeanUtils.copyProperties(stock, stockDto);
			stockDtoList.add(stockDto);
		}
		stockResponseDto.setStockList(stockDtoList);
		LOGGER.info("leaving getAllStocks() method");
		return stockResponseDto;
	}


	/**
	 * this method is used to update stock method.
	 */
	@Override
	public String updateStock(StockRequestDto stockRequestDto) {
		LOGGER.info("inside updateStock() method");
		Optional<Stock> optionalStock = stockRepository.findById(stockRequestDto.getStockId());
		if (!optionalStock.isPresent()) {
			LOGGER.error("error in update stock method");
			throw new StockNotFoundException();
		}
		Stock stock = optionalStock.get();
		if (stock.getQuantity() < stockRequestDto.getQuantity()) {
			LOGGER.error("error in update stock method");
			throw new QuantityInSufficientException();
		}
		if (AppConstants.BUY.equalsIgnoreCase(stockRequestDto.getTransactionType())) {
			stock.setQuantity(stock.getQuantity() - stockRequestDto.getQuantity());
		} else {
			stock.setQuantity(stock.getQuantity() + stockRequestDto.getQuantity());
		}
		try {
			stockRepository.save(stock);
		} catch (Exception ex) {
			LOGGER.error("error in update stock method");
			return AppConstants.FAILURE;
		}
		LOGGER.info("leaving updateStock() method");
		return AppConstants.SUCCESS;
	}

}
