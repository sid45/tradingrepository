package com.example.stockservice.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.stockservice.constants.Trend;

import lombok.Data;

@Data
@Entity
@Table(name="stock")
public class Stock {
	
	@Id
	private long stockId;
	
	private String stockName;
	
	private String stockDescription;
	
	private int quantity;
	
	private int priceDifference;
	
	private String trend;
	
	private double stockPrice;

}
