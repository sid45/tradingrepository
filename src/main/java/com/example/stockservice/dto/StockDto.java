package com.example.stockservice.dto;

import com.example.stockservice.constants.Trend;

public class StockDto {
	
	private long stockId;
	
	private String stockName;
	
	private String stockDescription;
	
	private int quantity;
	
	private int priceDifference;
	
	private String trend;
	
	private double stockPrice;

	public long getStockId() {
		return stockId;
	}

	public void setStockId(long stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockDescription() {
		return stockDescription;
	}

	public void setStockDescription(String stockDescription) {
		this.stockDescription = stockDescription;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPriceDifference() {
		return priceDifference;
	}

	public void setPriceDifference(int priceDifference) {
		this.priceDifference = priceDifference;
	}

	public String getTrend() {
		return trend;
	}

	public void setTrend(String trend) {
		this.trend = trend;
	}

	public double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
	
	
	

}
