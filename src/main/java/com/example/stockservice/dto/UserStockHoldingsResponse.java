package com.example.stockservice.dto;

import java.util.List;

public class UserStockHoldingsResponse {
	
	private List<StockHoldingDto> userStockHoldingsList;

	public List<StockHoldingDto> getUserStockHoldingsList() {
		return userStockHoldingsList;
	}

	public void setUserStockHoldingsList(List<StockHoldingDto> userStockHoldingsList) {
		this.userStockHoldingsList = userStockHoldingsList;
	}
	
	
	

}
