package com.example.stockservice.dto;

import java.util.List;

public class StockResponseDto {
	
	
	private List<StockDto> stockList;
	

	public List<StockDto> getStockList() {
		return stockList;
	}

	public void setStockList(List<StockDto> stockList) {
		this.stockList = stockList;
	}
	
	

}
