package com.example.stockservice.service;

import com.example.stockservice.dto.StockRequestDto;
import com.example.stockservice.dto.StockResponseDto;


public interface StockService {
	
	public StockResponseDto getAllStocks();
	
	public String updateStock(StockRequestDto stockRequestDto);
	
	
	

}
