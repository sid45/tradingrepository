package com.example.stockservice.constants;

public class AppConstants {
	
	public static final String SUCCESS = "stock has been updated successfully";
	
	public static final String FAILURE = "stock has not been updated successfully";

	public static final String STOCK_NOT_FOUND = "stock not found";

	public static final String QUANTITY_INSUFFICIENT = "insufficient quantity";
	
	public static final String BUY = "buy";
	
	

}
